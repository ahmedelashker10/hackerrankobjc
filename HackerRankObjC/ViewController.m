//
//  ViewController.m
//  HackerRankObjC
//
//  Created by Ahmed Elashker on 3/9/17.
//  Copyright © 2017 Ahmed Elashker. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (strong) NSString *firstName;
@property (strong) NSString *middleName;
@property (strong) NSString *lastName;
@property (readonly) NSString *fullName;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    
    NSArray *arr = [ViewController calculateDeltaEncoding:@[@25626, @25757, @24367, @24267, @16, @100, @2, @7277]];
    
    NSString *st = [ViewController polygonCountsFromLines:@[@"36 30 36 30",
                                                            @"15 15 15 15",
                                                            @"46 96 90 100",
                                                            @"86 86 86 86",
                                                            @"100 200 100 200",
                                                            @"-100 200 -100 200"]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSString *)fullName
{
    NSString *result = @"";
    if (![_firstName isEqualToString:@""] && _firstName != nil) {
        result = [NSString stringWithFormat:@"%@", _firstName];
    }
    if (![_middleName isEqualToString:@""] && _middleName != nil) {
        result = [NSString stringWithFormat:@"%@ %@", result, _middleName];
    }
    if (![_lastName isEqualToString:@""] && _lastName != nil) {
        result = [NSString stringWithFormat:@"%@ %@", result, _lastName];
    }
    return result;
}

+ (NSArray *)groupAnagrams:(NSArray *)lines {
    //enter your solution here:
    
    NSMutableArray *tempLines = [NSMutableArray arrayWithArray:lines];
    NSMutableArray *result = [[NSMutableArray alloc] init];
    
    int iterator = 0;
    while (iterator < tempLines.count) {
        NSString *str = [[tempLines objectAtIndex:iterator] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        NSString *sortedStr = [ViewController sortedString:str];
        
        bool matched = false;
        for (int i = iterator + 1; i < tempLines.count-1; i++) {
            NSString *nextStr = [[tempLines objectAtIndex:i] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            NSString *nextSortedStr = [ViewController sortedString:nextStr];
            
            if ([sortedStr isEqualToString:nextSortedStr]) {
                NSString *added = [NSString stringWithFormat:@"%@,%@", [tempLines objectAtIndex:iterator], [tempLines objectAtIndex:i]];
                [result addObject:added];
                [tempLines removeObjectAtIndex:i];
                [tempLines removeObjectAtIndex:iterator];
                matched = true;
            }
        }
        
        if (!matched) {
            iterator ++;
        }
    }
    
    return result;
}

+ (NSString *)sortedString:(NSString *)str {
    
    NSMutableArray *charArray = [NSMutableArray arrayWithCapacity:str.length];
    for (int i=0; i<str.length; ++i) {
        NSString *charStr = [str substringWithRange:NSMakeRange(i, 1)];
        [charArray addObject:charStr];
    }
    
    NSString *sortedStr = [[charArray sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] componentsJoinedByString:@""];
    
    return sortedStr;
}

+ (NSString *)polygonCountsFromLines:(NSArray *)lines {
    //enter your solution here:
    int square = 0;
    int rects = 0;
    int other = 0;
    for (NSString *str in lines) {
        NSArray *legs = [str componentsSeparatedByString:@" "];
        NSInteger A = [[legs objectAtIndex:0] integerValue];
        NSInteger B = [[legs objectAtIndex:1] integerValue];
        NSInteger C = [[legs objectAtIndex:2] integerValue];
        NSInteger D = [[legs objectAtIndex:3] integerValue];
        
        if (A < 1 || B < 1 || C < 1 || D < 1) {
            other += 1;
        }
        else if (A == B && B == C && C== D) {
            square += 1;
        }
        else if (A==C && B==D) {
            rects += 1;
        }
        else {
            other += 1;
        }
    }
    
    return [NSString stringWithFormat:@"%d %d %d", square, rects, other];
}

+ (NSArray *)calculateDeltaEncoding:(NSArray *)numbers {
    //enter your solution here:
    
    NSMutableArray *tempNumbers = [NSMutableArray arrayWithArray:numbers];
    
    for (int i = 1; i< tempNumbers.count; i+=1) {
        NSNumber *current = [numbers objectAtIndex:i];
        NSNumber *previous = [numbers objectAtIndex:i-1];
        NSInteger delta = current.integerValue - previous.integerValue;
        [tempNumbers replaceObjectAtIndex:i withObject:[NSNumber numberWithInteger:delta]];
    }
    
    int iterator = 1;
    
    while (iterator < tempNumbers.count) {
        if ([[tempNumbers objectAtIndex:iterator] integerValue] > 127) {
            [tempNumbers insertObject:[NSNumber numberWithInteger:-128] atIndex:iterator];
            iterator += 2;
        }
        else if ([[tempNumbers objectAtIndex:iterator] integerValue] < -127) {
            [tempNumbers insertObject:[NSNumber numberWithInteger:-128] atIndex:iterator];
            iterator += 2;
        }
        else {
            iterator += 1;
        }
    }
    
    return tempNumbers;
}

@end
