//
//  main.m
//  HackerRankObjC
//
//  Created by Ahmed Elashker on 3/9/17.
//  Copyright © 2017 Ahmed Elashker. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
