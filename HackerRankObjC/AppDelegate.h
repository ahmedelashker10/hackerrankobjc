//
//  AppDelegate.h
//  HackerRankObjC
//
//  Created by Ahmed Elashker on 3/9/17.
//  Copyright © 2017 Ahmed Elashker. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

